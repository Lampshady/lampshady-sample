class StaticPagesController < ApplicationController
  layout "default"

  def home
  end

  def help
  end

  def about
  end

  def contacts
  end

end
