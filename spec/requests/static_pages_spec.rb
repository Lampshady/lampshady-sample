require 'spec_helper'


describe "Static pages" do
  subject { page }

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    expect(page).to(have_title(full_title('About Us')))
    click_link "Help"
    expect(page).to(have_title(full_title('Help')))
    click_link "Contacts"
    expect(page).to(have_title(full_title('Contacts')))
    click_link "Home"
    click_link "Sign up now!"
    expect(page).to (have_title(full_title('Sign Up')))
    click_link "sample app"
    expect(page).to(have_title(full_title('')))
  end

  shared_examples_for "all static pages" do
    it { should have_selector('h1', text: heading) }
    it { should have_title(full_title(page_title)) }
  end

  describe "Home Page" do
    let(:heading){'Sample App'}
    let(:page_title){''}
    before {visit root_path}

    it_should_behave_like("all static pages")
    it { should_not have_title('| Home') }

  end


  describe "Help Page" do
    let(:heading){'Help'}
    let(:page_title){'Help'}
    before {visit help_path}

    it_should_behave_like("all static pages")

  end


  describe "About Page" do
    let(:heading){'About'}
    let(:page_title){'About Us'}
    before {visit about_path}

    it_should_behave_like("all static pages")
  end


  describe "Contacts" do
    let(:heading){'Contacts'}
    let(:page_title){'Contacts'}
    before {visit contacts_path}

    it_should_behave_like("all static pages")

  end

end
